
<html>

<head>
    <title>Edit News</title>

    <!--bootstrap css cdn-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--bootstrap js cdn-->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">NEWS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url('News'); ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                
            </ul>

        </div>
    </nav>
    
    <section id="news">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                <?php foreach ($news as $value) {
                    $news_id = $value['news_id'];
                    $news_title = $value['news_title'];
                    $news = $value['news'];
                    
                } ?>

                    <form class="needs-validation" action="<?php echo base_url('news/updatenews/'.$news_id); ?>" method="POST" enctype='multipart/form-data' novalidate>
                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo validation_errors('<p style="color: rgb(243, 103, 103)">', '</p>'); ?>
                        <div class="form-group">
                            <label for="usr">Title:</label>
                            <input type="text" class="form-control" id="titile" name="news_title" value="<?php echo $news_title; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="comment">News:</label>
                            <textarea class="form-control" rows="5" id="news" name="news"  required><?php echo $news; ?></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </section>


    <script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

</body>

</html>