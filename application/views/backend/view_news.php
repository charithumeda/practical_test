
<html>

<head>
    <title>View News</title>

    <!--bootstrap css cdn-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
    <!--bootstrap js cdn-->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <style>
    .addbtn {
        padding: 16px;
    }

    </style>


</head>

<body>
    
    <header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">NEWS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url('News'); ?>">Home <span class="sr-only">(current)</span></a>
                </li>
            </ul>

        </div>
    </nav>
    
    </header>

    <!-- Start Add news button Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 addbtn">
                <a href="<?php echo base_url('News/addnews'); ?>" class="btn btn-primary">Add News</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Add news button Section -->

    <!-- Start News Section -->
    <section id="news">

        <div class="container">

            <div class="row">

                <div class="col-md-12">
                    <div  id="myTable">
                    <table class="table" >
                        <thead class="thead-dark">
                           
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">News</th>
                                <th scope="col">Option</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <?php
                            $count = 0;
                            foreach($news as $data) {
                        ?>
                            <tr>
                                <th scope="row"><?php echo ++$count; ?>.</th>
                                <td><?php echo $data['news_title']; ?></td>
                                <td><?php echo $data['news']; ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>news/editnews/<?php echo $data['news_id']; ?>" class="btn btn-success">Edit</a>
                                    <a href="<?php echo base_url(); ?>news/deletenews/<?php echo $data['news_id']; ?>" data-news-id="<?php echo $data['news_id']; ?>" class="btn btn-danger newsConfirm">Delete</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        
                    </table>
                    <div>


                </div>

            </div>

        </div>
    </section>

    <div class="container">
    <nav aria-label="Page navigation">
        <ul class="pagination" id="pagination"></ul>
    </nav>
</div>

    <script type="text/javascript">
    $(function () {
        window.pagObj = $('#myTable').twbsPagination({
            totalPages: 5,
            visiblePages: 5,
            startPage: 1,
            onPageClick: function (event, page) {
                console.info(page + ' (from options)');
            }
        }).on('page', function (event, page) {
            console.info(page + ' (from event listening)');
        });
    });
</script>

    <script src="<?php echo base_url('assets/backend/'); ?>js/jquery.twbsPagination.js" type="text/javascript"></script>

</body>


</html>