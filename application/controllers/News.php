<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller{

	public function template($page, $mainData) {
		$this->load->view($page, $mainData);
	}

	// Start View News
	public function index() {
		$page = 'backend/view_news';
		$mainData = array(
			'news' => $this->News_model->Get_All('news'),
			);
		$this->template($page, $mainData);	
	}
	// End View News

	//Start Add News
	public function addnews(){
		$page = 'backend/add_news';
		$mainData = array(
			'pagetitle' => 'Add New News'
		);

		$this->template($page, $mainData);
	}
	//End Add News
	
	//Start Insert News
	public function insertnews(){
		$data = array(
			'news_title' =>$this->input->post('news_title'),
			'news' =>$this->input->post('news'),
			);
		$data1 = $this->News_model->insert($data, 'news');

		$this->session->set_flashdata('msg','<div class="alert alert-success">News Added Successfully!</div>');
		redirect('News/addnews');
	}
	//End Insert News

	//Start Edit News
	public function editnews() {
		$id = $this->uri->segment(3);
		$page = 'backend/edit_news';
		$mainData = array(
			'pagetitle' => 'Edit News',
			'news' => $this->News_model->Get_Single('news','news_id',$id),
			);

		$this->template($page, $mainData);	
	}
	//End Edit News

	//Start Update News
	public function updatenews(){
		$id = $this->uri->segment(3);
		$data = array(
			'news_title' =>$this->input->post('news_title'),
			'news' =>$this->input->post('news'),
			);
		
		$data1 = $this->News_model->update($data,'news','news_id',$id);
		$this->session->set_flashdata('msg','<div class="alert alert-success">News Updated Successfully!</div>');

		redirect('News/editnews/'.$id);
	}
	//End Update News

	//Start Delete News
	public function deletenews(){
		$id = $this->uri->segment(3);
		$this->News_model->delete('news','news_id',$id);
		redirect('News');
	}
	//End Delete News

}
