<?php
class News_model extends CI_Model{

    function Get_All($tablename)
	{
		$this->db->select('*');
		$this->db->from($tablename);
		$query = $this->db->get();
		return $query->result_array();
    }

    function insert($Data,$table_name)
	{
		$this->db->insert($table_name, $Data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
    }
    
    function Get_Single($table_name,$where,$id)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($where,$id);
		$query = $this->db->get();
		return $query->result_array();

	}
    
    function update($Data,$table_name,$where,$id)
	{
		$this->db->where($where, $id);
		$this->db->update($table_name, $Data);
	}

	function delete($table_name,$where,$id )
	{
		$this->db->where($where, $id);
		$this->db->delete($table_name);

	}

}